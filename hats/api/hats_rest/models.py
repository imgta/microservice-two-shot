from django.db import models

# Create your models here.
class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    class Meta:
        verbose_name = "LocationVO"
        verbose_name_plural = "LocationVOs"
        ordering = ("closet_name", "section_number", "shelf_number")

    def __str__(self):
        return self.closet_name

class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    image = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.PROTECT
    )

    class Meta:
        verbose_name = "Hat"
        verbose_name_plural = "Hats"

    def __str__(self):
        return f"{self.color} {self.style}"
