from django.contrib import admin
from .models import Hats, LocationVO


# Register your models here.
@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    pass
    # list_display = [
    #     "fabric",
    #     "style",
    #     "color",
    #     "image",
    #     "id",
    # ]


@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    pass
    # list_display = [
    #     "closet_name",
    #     "section_number",
    #     "shelf_number",
    #     "id",
    # ]
    # exclude = ["import_href"]
