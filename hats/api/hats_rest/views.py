from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Hats, LocationVO
from common.json import ModelEncoder
import json


# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "section_number", "shelf_number",]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = ["id", "color", "style", "fabric", "image"]

    def get_extra_data(self, obj):
        return {"location": obj.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "image",
        "location",
    ]
    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET","POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get Hat location object and put in data dictionary
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status = 400,
            )
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder = HatDetailEncoder,
            safe = False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            {"hat": hat},
            encoder = HatDetailEncoder,
            safe = False,
        )

    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location_href = content["location"]
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status = 400,
            )
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
