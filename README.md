# Wardrobify

Team:

* Person 1 - Gordon Ta - HATS microservice
* Person 2 - Kapil Adhikari SHOES microservice

## Design

## Shoes microservice

The following are the steps I took:
1. Installed Django app into django project settings:
 Added this line "shoes_rest.apps.ShoesApiConfig" to INSTALLED_APPS,
2. Created the models: BinVO and Shoe that are classes I made in models.
3. Developed view functions and encoders.
4. Added path to url.py, admin.py, and shoes_project/urls.py.
5. Launched Insomnia, entered the URLs, and tested the functions.
6. Implemented a polling feature to allow bin data to interact with shoes. On insomnia, I put it to the test by adding a new shoe to my shoelist.
7. Begun creating the frontend, introduced the ShoeList component, implemented cards and columns, with three columns in each row.
8. Added CreateShoeForm component, wrote jsx as return, gathered input data, and turned it into component states.
9. A nested route has been added to the App component.


## Hats microservice

Installed hats app and connected to the wardrobe microservice. Created hats and Location model, wrote encoders and view functions for all HTTP requests for lists and detail views, routed view functions to the appropriate URL paths, added host domains and updated allowed CORS origins for hats port (8090) before confirming all view functions worked via insomnia. Imported and leveraged LocationVO to create poller function to poll for Location data. Built out React components to fetch and display list/cards of hats (with delete functionality) and a hat creation form.
