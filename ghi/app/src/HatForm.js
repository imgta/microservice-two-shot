import React, {useEffect, useState} from 'react';

function HatForm() {
    const [formData, setData] = useState({
        style: "",
        fabric: "",
        color: "",
        image: "",
        locations: [],
        location: "",
    });

    const handleChange = (event) => {
        const { name, value } = event.target;
        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};
        delete data.locations;
        console.log(formData);

        try {
            const hatUrl = "http://localhost:8090/api/hats/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(hatUrl, fetchConfig);
            if(!response.ok) {
                throw new Error('Request for hats has failed')
            }
            const newHat = await response.json();
            console.log(newHat);

            setData({
                style: "",
                fabric: "",
                color: "",
                image: "",
                locations: [],
                location: "",
            });

    } catch (error) {
        console.error(error);
    }
    };
    useEffect(() => {
        const fetchLocations = async () => {
            try {
                const locationUrl = 'http://localhost:8100/api/locations/';
                const response = await fetch(locationUrl);

                if (!response.ok) {
                    throw new Error("Request for locations has failed");
                }

                const data = await response.json();
                setData((oldData) => ({ ...oldData, locations: data.locations}));
                console.log(data);

                } catch (error) {
                    console.error(error);
                }
        };

        fetchLocations();
    }, []);


return (
        <div className="row mt-5">
            <div className="col col-sm-auto">
                <img width="350" className="bg-white rounded shadow d-block mx-auto mb-2 pt-4" src="https://source.unsplash.com/350x350/?hats" alt="Banner" />
            </div>

            <div className="offset-0 col-8">
                <h2 className="text-center pb-2">Create a hat</h2>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input value={formData.style} onChange={handleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                        <label htmlFor="style">Style</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.color} onChange={handleChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.fabric} onChange={handleChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={formData.image} onChange={handleChange} placeholder="Image URL" type="text" name="image" id="image" className="form-control" />
                        <label htmlFor="image">Image URL</label>
                    </div>
                    <div className="mb-3">
                    <select value={formData.location} onChange={handleChange} required name="location" id="location" className="form-select">
                        <option value="">Choose a location</option>
                        {formData.locations.map(location => {
                        return (
                            <option key={location.href} value={location.href}>
                            {location.closet_name}
                            </option>
                        );
                        })}
                    </select>
                    </div>
                    <div id="button-pull">
                    <button className="btn btn-primary btn-block">Create</button>
                    </div>
                </form>
                </div>
            </div>
    );
}

export default HatForm;
