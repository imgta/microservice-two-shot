import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from 'react';
import MainPage from './MainPage';
import CreateShoeForm from './CreateShoeForm';
import ShoeList from './ShoeList';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';

// Destructure 'hats, shoes' objects within App function parameters
function App({hats, shoes}) {
  // Set hats and shoes to empty array if undefined so objects can always be iterable
  hats = hats || [];
  shoes = shoes || [];

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatList hats={hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="shoes">
            <Route path="" element={<ShoeList shoes={shoes} />} />
            <Route path="new" element={<CreateShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
