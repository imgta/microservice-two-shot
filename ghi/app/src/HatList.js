import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import './hat.css';
import HatScript from './HatScript'

function HatList() {
    const [hats, setHats] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const url = 'http://localhost:8090/api/hats';
                const response = await fetch(url);

                if (response.ok) {
                    const data = await response.json();
                    setHats(data.hats);
                }
            } catch (error) {
                console.error(error);
            }
        };

        fetchData();
    }, []);

    // DELETE FUNCTION
    async function deleteHat(hat_id) {
        const deleteUrl = `http://localhost:8090/api/hats/${hat_id}/`;
        const fetchConfig = {method: "DELETE",};
        const response = await fetch(deleteUrl, fetchConfig);
        if (response.ok) {
            console.log("Hat has been deleted", response);
            window.location.reload(false);
        }
    }


// ######## FOR KAPIL #########
    // return (
    //     <table className="table table-striped table-hover text-center">
    //     <thead>
    //     <tr>
    //         <th>Color</th>
    //         <th>Style</th>
    //         <th>Fabric</th>
    //         <th>Image</th>
    //         <th>Location</th>
    //         <th>Delete</th>
    //     </tr>
    //     </thead>
    //     <tbody>
    //         {props.hats.map(hat => {
    //             return (
    //             <tr key={hat.id}>
    //                 <td>{ hat.color }</td>
    //                 <td>{ hat.style }</td>
    //                 <td>{ hat.fabric }</td>
    //                 <td><img src={ hat.image } alt="hat pic" width="150" height="150" /></td>
    //                 <td>{ hat.location }</td>
    //                 <td><button onClick={() => deleteHat(hat.id)} type="button" className="btn btn-danger">Delete?</button></td>
    //             </tr>
    //             );
    //         })}
    //     </tbody>
    // </table>
    // );


    // Converted hat.js script

    const [zIndex, setZIndex] = useState(20);
    const [isShowing, setIsShowing] = useState(false);
    const handleClick = (e) => {
    e.preventDefault();
    setIsShowing((prevIsShowing) => !prevIsShowing);
    setZIndex((prevZIndex) => prevZIndex + 1);
    };

    return (
    <>
                <h2 className="text-center">Headwear Collection</h2>
    <div className="cards">
    {hats.map(hat => {
    return (
        <div className="card" key={hat.id}>
        <div className="card__image-holder">
        <img className="card__image object-fit-contain" src={hat.image} alt="hat pic" width="300" height="225" />
        </div>
        <div className="card-title">
        <Link to="#" className="toggle-info btn">
        <span className="left"></span>
        <span className="right"></span>
        </Link>
        <h2>
            {hat.style}
            <small className="text-secondary fs-6 fw-medium">From: {hat.location}</small>
        </h2>
    </div>
    <div className="card-flap flap1">
        <div className="card-description">
        <div className="text-primary fst-italic lh-1 fw-medium">{hat.color}, 100% {hat.fabric}</div>
        <br></br>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quidem labore quia aperiam, pariatur nesciunt non nisi repudiandae cupiditate. Porro accusamus qui provident itaque dolor reprehenderit ducimus eum, explicabo aut obcaecati!
        </div>
        <div className="card-flap flap2">
        <div className="card-actions">
            <button onClick={() => deleteHat(hat.id)} type="button" className="btn btn-danger">Delete?</button>
        </div>
        </div>
    </div>
    </div>
    );
    })}
    </div>
    <HatScript />
    </>
    )}


export default HatList;
