import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';


function ShoesList(props) {
    const deleteShoe = async (shoe) => {
        const shoesUrl = `http://localhost:8080/api/shoes/${shoe.id}/`
        const fetchConfig = {method: "delete"}
        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok) {
            console.log("Shoe has been deleted", response);
            window.location.reload(false);

        }
    }

    if (props.shoes === undefined) {
        return null;
    }

    return (
        <table className="table table-striped table-hover text-center">
            <thead>
            <tr>
                <th>Manufacturer Name</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Closet Name</th>
            </tr>
            </thead>
            <tbody>
            {props.shoes.map(shoe => {
                return (
                <tr key={shoe.id}>
                    <td>{ shoe.manufacturer_name }</td>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.color }</td>
                    <td>
                        <img
                            src={shoe.picture_url}
                            alt=""
                            width="75px"
                            height="75px"
                        />
                    </td>
                    <td>{ shoe.closet_name }</td>
                    <td>
                        <button onClick={() => deleteShoe(shoe.id)} type="button" className="btn btn-danger">Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    );
}
export default ShoesList;