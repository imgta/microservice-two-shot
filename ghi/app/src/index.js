import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';


const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadHats() {
  const responseHat = await fetch('http://localhost:8090/api/hats/');
  if (responseHat.ok) {
    const dataHat = await responseHat.json();
    console.log(dataHat)
    root.render(
      <React.StrictMode>
        <App hats={dataHat.hats} />
      </React.StrictMode>
    );
  } else {
    console.error(responseHat);
  }
}
loadHats();

async function loadShoes() {
  const responseShoe = await fetch('http://localhost:8080/api/shoes/');
  if (responseShoe.ok) {
    const dataShoe = await responseShoe.json();
    console.log(dataShoe)
    root.render(
      <React.StrictMode>
        <App shoes={dataShoe.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(responseShoe);
  }
}
loadShoes();
