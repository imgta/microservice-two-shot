// $(document).ready(function(){
//     var zindex = 20;

//     $("div.card").click(function(e){
//     e.preventDefault();

//     var isShowing = false;

//     if ($(this).hasClass("show")) {
//     isShowing = true
//     }

//     if ($("div.cards").hasClass("showing")) {
//     // a card is already in view
//     $("div.card.show")
//         .removeClass("show");

//     if (isShowing) {
//         // this card was showing - reset the grid
//         $("div.cards")
//         .removeClass("showing");
//     } else {
//         // this card isn't showing - get in with it
//         $(this)
//         .css({zIndex: zindex})
//         .addClass("show");

//     }

//     zindex++;

//     } else {
//     // no cards in view
//     $("div.cards")
//         .addClass("showing");
//     $(this)
//         .css({zIndex:zindex})
//         .addClass("show");

//     zindex++;
//     }

// });
// });

import React, { useEffect } from 'react';

function HatScript() {
useEffect(() => {
const handleClick = (e) => {
    const card = e.target.closest('.card');
    if (!card) return;

    e.preventDefault();
    const isShowing = card.classList.contains('show');
    const cardsContainer = document.querySelector('.cards');

    if (cardsContainer.classList.contains('showing')) {
    // A card is already in view
    const showingCard = cardsContainer.querySelector('.card.show');
    showingCard?.classList.remove('show');

    if (isShowing) {
        // This card was showing - reset the grid
        cardsContainer.classList.remove('showing');
    } else {
        // This card isn't showing - show it
        card.style.zIndex = 20;
        card.classList.add('show');
    }
    } else {
    // No cards in view
    cardsContainer.classList.add('showing');
    card.style.zIndex = 20;
    card.classList.add('show');
    }
};

document.addEventListener('click', handleClick);

return () => {
    document.removeEventListener('click', handleClick);
};
}, []);

return null;
}

export default HatScript;
