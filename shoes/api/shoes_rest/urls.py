from django.urls import path
from django.contrib import admin
from .views import list_shoes, shoe_detail

urlpatterns = [
    path('admin/', admin.site.urls),
    path("shoes/", list_shoes, name="list_shoes"),
    path("shoes/<int:pk>/", shoe_detail, name="shoe_detail")
]
